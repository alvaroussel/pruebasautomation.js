//Acceso a la aplicación
var target = UIATarget.localTarget();
//Acceso a la pantalla de la aplicaciónvar aplicacion = target.frontMostApp();
//Acceso a la vista principal de la aplicaciónvar vistaPrincipal = aplicacion.mainWindow();

// Coges las celdas como un array y de esta forma con la longitud
// del array se puede recorrer todas las celdas de la tabla. 
var sizeCeldas = vistaPrincipal.tableViews()[0].cells();

//Recorre todos los elementos de la vista principal
for (var i = 0; i < sizeCeldas.length; i++){
	//Imprimer un mensaje en la pantalla del Log Messages
	UIALogger.logStart("Celda de tabla '"+i+"' ");
    //Pulsa a la celda de la posicion i
	vistaPrincipal.tableViews()[0].cells()[i].tap();
    //Muestra el árbol de elementos
	vistaPrincipal.logElementTree();
    //Pulsa el botón izquierdo (Juagdores) del navigationBar
	vistaPrincipal.navigationBar().leftButton().tap();
	
	//En caso de haber recorrido los 4 primeros elementos
	if (i == 4){
		//Espera de un segundo
		UIATarget.localTarget().delay(1);
        //Hace un scroll(deslizamiento) hasta el elemento con nombre "Zidane"
		vistaPrincipal.tableViews()[0].scrollToElementWithPredicate("name beginsWith 'Zidane'");
		//Espera de un segundo
		UIATarget.localTarget().delay(1);
	}
	//Imprime un mensaje al final de la pantalla del Log Messages
	UIALogger.logPass("Fin celda de tabla");
}